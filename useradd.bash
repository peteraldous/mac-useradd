#!/bin/bash
set -o nounset;
set -o errexit;

username="$1";
name="$2";

# TODO: make sure there are only valid chars in the username
maxid=$(dscl . -list /Users UniqueID | awk '{print $2}' | sort -ug | tail -1)
user_id=$((maxid+1))
GUID=20

sudo -v;
sudo dscl . -create /Users/$username ;
sudo dscl . -create /Users/$username UserShell /bin/bash ;
sudo dscl . -create /Users/$username RealName $name ;
sudo dscl . -create /Users/$username UniqueID $user_id ;
sudo dscl . -create /Users/$username PrimaryGroupID $GUID ;
sudo dscl . -passwd /Users/$username changeme ;
sudo mkdir /Users/$username ;
sudo chown $username:staff /Users/$username ;
sudo dscl . -create /Users/$username NFSHomeDirectory /Users/$username ;
sudo dscl . -create /Users/$username dsAttrTypeNative:_writers_hint $username
sudo dscl . -create /Users/$username dsAttrTypeNative:_writers_jpegphoto $username
sudo dscl . -create /Users/$username dsAttrTypeNative:_writers_passwd $username
sudo dscl . -create /Users/$username dsAttrTypeNative:_writers_picture $username
sudo dscl . -create /Users/$username dsAttrTypeNative:_writers_realname $username
