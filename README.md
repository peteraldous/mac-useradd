Mac-Useradd: A utility for Mac servers
======================================

For those who like the Mac way of doing things, Macs are great. But Mac servers simply don't seem to have been designed for remote access and for doing things without a mouse. I found simple tasks like adding users to be complicated. Unfortunately, the process is brittle, as the details of these commands can change with updates to OS X. This script, however, is working for now and I'm happy to share it with others in case they need to do something similar.

At present, I trust myself to use safe usernames and such, so I don't validate the parameters to the program. If you'd like, you're welcome to submit a pull request.

I won't even bother putting a GPL license on this. You're welcome to do anything you like with it - use it for open source or proprietary things, for profit or not.
